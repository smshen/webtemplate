WebTemplate
===========

A website template created by gulp with jade, livescript, stylus

Installation
===========
`npm install && bower install`

Usage
=====
For development

`gulp watch`

For deployment

`gulp serve`